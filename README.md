# harbour-themepack-newaita
Newaita icon theme for Sailfish OS

Source: https://framagit.org/ggenois/harbour-themepack-newaita

Icons from https://www.gnome-look.org/p/1243493/

Build icon theme:
``rpmbuild --bb --target noarch harbour-themepack-newaita.spec``
