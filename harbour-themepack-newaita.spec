Name:          harbour-themepack-newaita
Version:       0.0.9
Release:       1
Summary:       Newaita icon pack
Vendor:        Dmitry Z.
Requires:      sailfish-version >= 2.0.1, harbour-themepacksupport >= 0.0.8-1
Packager:      dfstorm <dfstorm@riseup.net>
URL:           https://git.geno.is/root/harbour-themepack-newaita
License:       GPL

%description
Newaita icon pack for Sailfish OS.

%files
%defattr(-,root,root,-)
/usr/share/*

%postun
if [ $1 = 0 ]; then
    // Do stuff specific to uninstalls
rm -rf /usr/share/harbour-themepack-newaita
else
if [ $1 = 1 ]; then
    // Do stuff specific to upgrades
echo "Upgrading"
fi
fi

%changelog

* Tue Oct 22 2019 0.0.9
- Updating dynclock icon.

* Tue Oct 22 2019 0.0.8
- Adding dyncal with holydays

* Tue Oct 19 2019 0.0.7
- Adding icons (communi, depecher,  Unplayer, VLC Remote, Tremotesf, Charge Monitor, Themes, UI Themer (...))

* Tue Oct 19 2019 0.0.6
- Ajusting folder icons. Adding dynclock.

* Tue Oct 18 2019 0.0.5
- Adding folder icons.

* Tue Oct 18 2019 0.0.4
- Adding 1 icon (Phone), modifying 3 icons. (SMS, Storeman and GPSInfo)

* Tue Oct 17 2019 0.0.3
- Adding 3 icons. (harbour-systemmonitor, harbour-recorder, toeterm)

* Tue Oct 11 2019 0.0.2
- Adding 6 icons. (jollacompass , file-browser, storeman, pure-maps, books and gpsinfo)

* Tue Oct 9 2019 0.0.1
- First build.

